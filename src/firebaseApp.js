import { initializeApp } from "firebase/app"
import {getAuth, signInWithEmailAndPassword} from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCouujpZfWwT57jqrRYA8nfXFom0arCO-w",
    authDomain: "chat-d8b12.firebaseapp.com",
    databaseURL: "https://chat-d8b12-default-rtdb.firebaseio.com",
    projectId: "chat-d8b12",
    storageBucket: "chat-d8b12.appspot.com",
    messagingSenderId: "1014954227687",
    appId: "1:1014954227687:web:1fa407e7a9f6157fffaf42"
};
initializeApp(firebaseConfig)


function fireBaseAuthentication(store){
    return signInWithEmailAndPassword(getAuth(),store.getState().email, store.getState().password)
        .then((userCredential) => {
            return userCredential.user
        })
        .catch(() => {
            return false
        })
}


export default fireBaseAuthentication
