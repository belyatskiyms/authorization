import React from'react'
import { configure, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import LoginForm from './LoginForm';
import {Provider} from 'react-redux'
import store from "./sagas";
configure({ adapter: new Adapter() });


it('component form rendered', ()=>{
    const myComponent = mount(
        <Provider store={store}>
            <LoginForm/>
        </Provider>)
    expect(myComponent.find('form')).toHaveLength(1)
})

