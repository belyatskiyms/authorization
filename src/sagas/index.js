import {call, put, takeLatest} from "redux-saga/effects";
import fireBaseAuthentication from "../firebaseApp";
import createSagaMiddleware from "redux-saga";
import {applyMiddleware, createStore} from "redux";

// Sagas
function* watchFetch() {
    yield takeLatest('FETCH_MESSAGES_REQUEST', fetchAsync);
}

function* fetchAsync() {
    const data = yield call(() => {
        return fireBaseAuthentication(store)
    })
    if (!data){
        yield put(FETCH_MESSAGES_FAILURE());
    }else{
        yield put(FETCH_MESSAGES_SUCCESS());
    }
}


const initialState = {
    message: '',
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_MESSAGES_REQUEST':
            return {
                email: action.email,
                password: action.password,
            };
        case 'FETCH_MESSAGES_SUCCESS':
            return {
                message: 'Вы успешно авторизовались',
            };
        case 'FETCH_MESSAGES_FAILURE':
            return {
                message: 'Неправильный логин или пароль',
            };
        default:
            return state;
    }
};
// Action Creators
const FETCH_MESSAGES_SUCCESS= () => {
    return { type: 'FETCH_MESSAGES_SUCCESS' }
};
const FETCH_MESSAGES_FAILURE = () => {
    return { type: 'FETCH_MESSAGES_FAILURE' }
};

// Store
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware)
);
sagaMiddleware.run(watchFetch);

export default store