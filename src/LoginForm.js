import './LoginForm.css';
import { useFormik } from 'formik';
import {connect} from 'react-redux'
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function validate(values) {
    const errors = {};
    if (!values.email) {
        errors.email = 'Это поле обязательно для заполнения';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Некорректный email адресс';
    }
    if (!values.password) {
        errors.password = 'Это поле обязательно для заполнения';
    }
    return errors;
}

function LoginForm(props) {
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validate,
        onSubmit: values => {
            props.authentication(values.email, values.password)
        },
    });
    return (
        <form noValidate onSubmit={formik.handleSubmit} className={"login-page__form"}>
            <label className={"login-page__form-label"} htmlFor="email">Логин (email)</label>
            <input
                className={"login-page__form-input"}
                id="email"
                name="email"
                type="email"
                onChange={formik.handleChange}
                value={formik.values.email}
                onBlur={formik.handleBlur}
            />
            {formik.touched.email && formik.errors.email ?
                <div className={"login-page__error-message"}>
                    <FontAwesomeIcon icon={faExclamationTriangle } />     {formik.errors.email}
                </div> : null}
            <label className={"login-page__form-label"} htmlFor="password">Пароль</label>
            <input
                className={"login-page__form-input"}
                id="password"
                name="password"
                type="password"
                onChange={formik.handleChange}
                value={formik.values.password}
                onBlur={formik.handleBlur}
            />
            {formik.touched.password && formik.errors.password ?
                <div className={"login-page__error-message"}>
                    <FontAwesomeIcon icon={faExclamationTriangle } />     {formik.errors.password}
                </div> : null}
            <div className={"login-page__error-message"}>{props.message}</div>
            <button className={"login-page__form-button"} type="submit">Авторизоваться</button>
        </form>
    );
}

export default connect(
    state => {
        return state
    },
    dispatch => ({
        authentication: (vEmail, vPassword) => {
            dispatch({type: 'FETCH_MESSAGES_REQUEST', email:vEmail,password:vPassword})
        }
    })
)(LoginForm);
