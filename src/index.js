import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import LoginForm from './LoginForm';
import {Provider} from 'react-redux'
import store from './sagas/index'

ReactDOM.render(
    <Provider store={store}>
        <LoginForm/>
    </Provider>,
  document.getElementById('login-page')
);
